﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Domain;

namespace API.Controllers
{
    [Authorize]
    public class LeaguesController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Leagues
        public IQueryable<Leagues> GetLeagues()
        {
            return db.Leagues;
        }

        // GET: api/Leagues/5
        [ResponseType(typeof(Leagues))]
        public async Task<IHttpActionResult> GetLeagues(int id)
        {
            Leagues leagues = await db.Leagues.FindAsync(id);
            if (leagues == null)
            {
                return NotFound();
            }

            return Ok(leagues);
        }

        // PUT: api/Leagues/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLeagues(int id, Leagues leagues)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != leagues.LeagueId)
            {
                return BadRequest();
            }

            db.Entry(leagues).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LeaguesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Leagues
        [ResponseType(typeof(Leagues))]
        public async Task<IHttpActionResult> PostLeagues(Leagues leagues)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Leagues.Add(leagues);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = leagues.LeagueId }, leagues);
        }

        // DELETE: api/Leagues/5
        [ResponseType(typeof(Leagues))]
        public async Task<IHttpActionResult> DeleteLeagues(int id)
        {
            Leagues leagues = await db.Leagues.FindAsync(id);
            if (leagues == null)
            {
                return NotFound();
            }

            db.Leagues.Remove(leagues);
            await db.SaveChangesAsync();

            return Ok(leagues);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LeaguesExists(int id)
        {
            return db.Leagues.Count(e => e.LeagueId == id) > 0;
        }
    }
}